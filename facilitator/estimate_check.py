import datetime
import time
import string
import sys
import os
import telebot

from database import db

token = "5302941769:AAGNQ8PH2ebTx38rXkNxWDWcdGI3mgizyyg"
bot = telebot.TeleBot(token)
prev_users = []
# bot.config["api_key"] = token

def notify_start(user_id: int, estimator_number: int):

	if estimator_number is None:
		return

	prev_estimator_number = estimator_number

	while True:
		time.sleep(30)
		q = "SELECT learn_time_estimate, estimator_number FROM users WHERE telegram_user_id={}".format(user_id)
		db.connect()
		new_estimate = db.execute_query(q , return_values=True)
		db.close()
		print(new_estimate, prev_estimator_number)
		estimator_number = new_estimate[0][1]
		if prev_estimator_number==estimator_number:
			bot.send_message(user_id,
							 "⏰ Твои товарищи по комнате оценили твою тему в {} часов {} минут".format(
								 new_estimate[0][0] // 60, new_estimate[0][0] - (new_estimate[0][0] // 60) * 60))
			break
		prev_estimator_number = estimator_number

while True:

	q = "SELECT * FROM users WHERE room_id is not NULL"
	db.connect()
	users = db.execute_query(q, return_values=True)
	db.close()


	"""
	Трекаем новых участников групп, и говорим сколько им дали времени на работу
	"""
	if len(prev_users) != len(users):
		for user in users:
			if user not in prev_users:
				notify_start(user_id=user[0], estimator_number=user[7])

	if users:

		for user in users:
			time_spend = (datetime.datetime.now() - user[5]).seconds // 60 % 60
			if time_spend > user[6]:
				bot.send_message(user[0], "⏰ Время на ботание закончилось. Как ваши успехи?")
				UPDATE_ESTIMATE = string.Template(
				"UPDATE users"
				" SET"
				" learn_time_estimate=$learn_time_estimate"
				" WHERE telegram_user_id=$telegram_user_id"
				)
				room_mates = db.get_room_members(user[2])
				user_string = user[1] + "  " + str(user[-1]) + "🧠 " + str(user[-2]) + "🎓"
				for mate in room_mates:
					if mate != user[0]:
						bot.send_message(mate, "🎺 У {} закончилось время. Задайте ему вопрос.".format(user_string) )
				q = UPDATE_ESTIMATE.substitute(telegram_user_id=user[0], learn_time_estimate=user[6] + 60)
				db.connect()
				db.execute_query(q)
				db.close()

			# За 15 минут до конца напоминаем о дедлайне
			elif (user[6] - time_spend < 15) and (user[6] - time_spend > 14) and ((datetime.datetime.now() - user[5]).seconds % 100 >= 45):
				bot.send_message(user[0] , "⏰ Через 15 минут ребята зададут тебе вопросы")

	prev_users = users

	time.sleep(10)