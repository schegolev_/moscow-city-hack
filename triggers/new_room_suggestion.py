import telebot

from telebot import types

token = "5302941769:AAGNQ8PH2ebTx38rXkNxWDWcdGI3mgizyyg"
bot = telebot.TeleBot(token)

USER_ID = 182495307
NEW_ROOM_NAME = "Преобразование Фурье"


markup = types.ReplyKeyboardMarkup(row_width=2)
itembtn1 = types.KeyboardButton('Нет')  # Будет просто игнорироваться в broadcaster'e (ничего не будет происходить)
itembtn2 = types.KeyboardButton(f'Да, кидай меня в "{NEW_ROOM_NAME}"')  # Будет перекидывать в другую комнату
itembtn3 = types.KeyboardButton(f'Да, кидай меня в "{NEW_ROOM_NAME}", но оставь и в текущей')  # Будет перекидывать в другую комнату.
                                                                     # Оставлять в предыдущей не будет, потому что это усложнение
markup.add(itembtn1, itembtn2, itembtn3)

msg = bot.send_message(USER_ID,
                       text="Ты сейчас разбираешься в преобразовании Фурье?"
                            "Кинуть тебя в боталку по теме?",
                       reply_markup=markup
                       )
