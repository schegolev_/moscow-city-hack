import string

import psycopg2 as pg2


class Database:
    def __init__(self, db, username, password, port, host):
        self.db = db
        self.username = username
        self.host = host
        self.password = password
        self.port = port
        self.cur = None
        self.conn = None

    def connect(self):
        self.conn = pg2.connect(host=self.host,
                                database=self.db,
                                user=self.username,
                                password=self.password,
                                port=self.port)
        self.cur = self.conn.cursor()

    def execute_query(self, query, return_values=False):
        self.cur.execute(query)
        if return_values:
            return self.cur.fetchall()
        self.conn.commit()

    def fetch_all(self, query):
        self.cur.execute(query)
        result = [r[0] for r in self.cur.fetchall()]
        return result

    def close(self):
        self.cur.close()
        self.conn.close()

    def create_user(self, telegram_user_id, telegram_name):
        USER_CREATE_TEMPLATE = string.Template(
            "INSERT INTO users(telegram_user_id, telegram_name)"
            " VALUES($telegram_user_id, '$telegram_name')"
        )
        i = USER_CREATE_TEMPLATE.substitute(telegram_user_id=telegram_user_id, telegram_name=telegram_name)
        self.connect()
        self.execute_query(i)
        self.close()

    def create_room(self, telegram_user_id, room_name):
        ROOM_CREATE_TEMPLATE = string.Template(
            "WITH counted AS ("
            "  SELECT nextval('rooms') AS rooms_count"
            " )"
            " UPDATE users"
            " SET"
            " room_id = c.rooms_count,"
            " room_name = '$room_name'"
            " FROM counted c"
            " WHERE users.telegram_user_id = $user_id"
        )
        q = ROOM_CREATE_TEMPLATE.substitute(room_name=room_name, user_id=telegram_user_id)
        self.connect()
        self.execute_query(q)
        room_id = self.execute_query("SELECT currval('rooms')", return_values=True)[0]
        self.close()
        return room_id[0]

    def get_room_id(self, room_name):
        ROOM_GET_TEMPLATE = string.Template(
            "SELECT DISTINCT room_id, count(room_id) AS users_amount"
            " FROM users"
            " GROUP BY room_id, room_name"
            " HAVING users.room_name = '$room_name'"
        )
        q = ROOM_GET_TEMPLATE.substitute(room_name=room_name)
        self.connect()
        idxs = self.execute_query(q, return_values=True)
        for idx in idxs:
            if 1 != 1:  # idx[-1] > 5: -- users in room limit
                continue
            else:
                return idx[0]
        else:
            return None

    def append_to_room(self, room_id, room_name, telegram_user_id, theme_name):
        ROOM_APPEND_TEMPLATE = string.Template(
            "UPDATE users"
            " SET"
            " room_id = $room_id,"
            " room_name = '$room_name',"
            " user_query = '$user_query'"
            " WHERE users.telegram_user_id = $user_id"
        )
        q = ROOM_APPEND_TEMPLATE.substitute(room_id=room_id,
                                            room_name=room_name,
                                            user_id=telegram_user_id,
                                            user_query=theme_name)
        self.connect()
        self.execute_query(q)
        self.close()

    def get_room_members(self, room_id) -> list:
        ROOM_MEMBERS_TEMPLATE = string.Template(
            "SELECT telegram_user_id"
            " FROM users"
            " WHERE room_id=$room_id"
        )
        q = ROOM_MEMBERS_TEMPLATE.substitute(room_id=room_id)
        self.connect()
        ids = self.execute_query(q, return_values=True)
        self.close()
        return [i[0] for i in ids]

    def get_room_by_user(self, telegram_user_id):
        ROOM_MEMBERS_TEMPLATE = string.Template(
            "SELECT room_id"
            " FROM users"
            " WHERE telegram_user_id=$telegram_user_id"
        )
        q = ROOM_MEMBERS_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        room_id = self.execute_query(q, return_values=True)[0][0]
        self.close()
        return room_id

    def get_user_name(self, telegram_user_id):
        USER_NAME_TEMPLATE = string.Template(
            "SELECT telegram_name"
            " FROM users"
            " WHERE telegram_user_id=$telegram_user_id"
        )
        q = USER_NAME_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        telegram_name = self.execute_query(q, return_values=True)[0][0]
        self.close()
        return telegram_name

    def get_user_theme(self, telegram_user_id):
        USER_NAME_TEMPLATE = string.Template(
            "SELECT user_query"
            " FROM users"
            " WHERE telegram_user_id=$telegram_user_id"
        )
        q = USER_NAME_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        user_query = self.execute_query(q, return_values=True)[0][0]
        self.close()
        return user_query


db = Database(host="rc1b-kujwcjnpecm2ix4k.mdb.yandexcloud.net",
              db='hack',
              username="hack",
              password="hackhack",
              port=6432)
