
import telebot
from typing import List
import functools

from database import db
from indexer import get_theme

from message_handlers_functions import (
    add_user_to_room_or_suggest_create_new_or_decline, ask_estimate
)


def send_all(message: telebot.types.Message, bot: telebot.TeleBot, room_id: int, text: str):
    user_ids = db.get_room_members(room_id)
    # Удаляем себя из рассылки
    user_ids.remove(message.from_user.id)

    for user_id in user_ids:
            bot.send_message(user_id, text)

    if message.voice:
        for user_id in user_ids:
            bot.send_voice(user_id, message.voice.file_id)
    
    if message.photo:
        for user_id in user_ids:
            bot.send_photo(user_id, message.photo[0].file_id)


def broadcast(message: telebot.types.Message, bot: telebot.TeleBot):
    """
    Берет сообщение и пересылает его всем в комнате.
    Отправителю сообщение не пересылается. 
    """
    # Первое, что делаем - забираем стейт из базы! На основе этого уже дальше логика строится.
    user_state, room_id = db.get_user_state(message.from_user.id)

    if user_state == "no_room":  # человек пытается попасть в комнату, None -- не инициализирован
        add_user_to_room_or_suggest_create_new_or_decline(message, bot)

    elif user_state == 'in_room':  # человек уже состоит в комнате

        if message.reply_to_message and 'Оценка: +' in message.text:
            previous_sender_name = message.reply_to_message.text.split(' ')[0]
            previous_sender_user = db.get_user_by_name(previous_sender_name)[0]
            db.update_user_answered_score(previous_sender_user, 1)
            db.update_user_asked_score(message.from_user.id, 1)
        else:
            user_info = db.get_user(telegram_user_id=message.from_user.id)
            user_string = user_info[1] + "  " + str(user_info[-1]) + "🧠 " + str(user_info[-2]) + "🎓"
            if not message.voice and not message.photo:
                text = f"{user_string}:\n" + message.text
            else:
                text = f"{user_string}"
            send_all(message, bot, room_id, text)

    else:
        bot.send_message(message.from_user.id, 'У нас неполадки, ваш state: {}'.format(bot.get_state(message.from_user.id)))
