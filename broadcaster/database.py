import string
import datetime

import psycopg2 as pg2

from typing import List, Tuple

class Database:
    def __init__(self, db, username, password, port, host):
        self.db = db
        self.username = username
        self.host = host
        self.password = password
        self.port = port
        self.cur = None
        self.conn = None

    def connect(self):
        self.conn = pg2.connect(host=self.host,
                                database=self.db,
                                user=self.username,
                                password=self.password,
                                port=self.port)
        self.cur = self.conn.cursor()

    def execute_query(self, query, return_values=False):
        self.cur.execute(query)
        if return_values:
            return self.cur.fetchall()
        self.conn.commit()

    def fetch_all(self, query):
        self.cur.execute(query)
        result = [r[0] for r in self.cur.fetchall()]
        return result

    def close(self):
        self.cur.close()
        self.conn.close()

##########################################
# ОПЕРАЦИИ С ПОЛЬЗОВАТЕЛЕМ
##########################################


    def create_user(self, telegram_user_id, telegram_name):
        USER_CREATE_TEMPLATE = string.Template(
          "INSERT INTO users(telegram_user_id, telegram_name, state)"
          " VALUES($telegram_user_id, '$telegram_name', 'no_room')"
        )
        i = USER_CREATE_TEMPLATE.substitute(telegram_user_id=telegram_user_id, telegram_name=telegram_name)
        self.connect()
        self.execute_query(i)
        self.close()

    def user_exists(self, telegram_user_id):
        USER_EXISTS = string.Template(
            "SELECT telegram_user_id"
            " FROM users"
            " WHERE telegram_user_id=$telegram_user_id"
            )
        q = USER_EXISTS.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        user_id = self.execute_query(q)
        self.close()
        if user_id:
            return True
        else:
            return False

    def get_user(self, telegram_user_id):
        GET_USER_TEMPLATE = string.Template(
          "SELECT *"
          " FROM users"
          " WHERE telegram_user_id=$telegram_user_id"
        )
        q = GET_USER_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        user_row = self.execute_query(q, return_values=True)[0]
        self.close()
        return user_row

    def get_user_by_name(self, telegram_name):
        GET_USER_TEMPLATE = string.Template(
          "SELECT *"
          " FROM users"
          " WHERE telegram_name='$telegram_name'"
        )
        q = GET_USER_TEMPLATE.substitute(telegram_name=telegram_name)
        self.connect()
        user_row = self.execute_query(q, return_values=True)[0]
        self.close()
        return user_row

    def update_user_answered_score(self, telegram_user_id, increment):
        UPDATE_USER_TEMPLATE = string.Template(
            "UPDATE users"
            " SET"
            " answered = answered + $increment"
            " WHERE users.telegram_user_id = $telegram_user_id"
        )
        q = UPDATE_USER_TEMPLATE.substitute(increment=increment, telegram_user_id=telegram_user_id)
        self.connect()
        self.execute_query(q)
        self.close()

    def update_user_asked_score(self, telegram_user_id, increment):
        UPDATE_USER_TEMPLATE = string.Template(
            "UPDATE users"
            " SET"
            " asked = asked + $increment"
            " WHERE users.telegram_user_id = $telegram_user_id"
        )
        q = UPDATE_USER_TEMPLATE.substitute(increment=increment, telegram_user_id=telegram_user_id)
        self.connect()
        self.execute_query(q)
        self.close()

    def update_user_time_estimation(self, telegram_user_id,
                                    new_time_estimation, new_estimator_number):
        UPDATE_USER_TEMPLATE = string.Template(
            "UPDATE users"
            " SET"
            " learn_time_estimate = $learn_time_estimate,"
            " estimator_number = $estimator_number"
            " WHERE users.telegram_user_id = $telegram_user_id"
        )
        q = UPDATE_USER_TEMPLATE.substitute(
            learn_time_estimate=new_time_estimation,
            estimator_number=new_estimator_number,
            telegram_user_id=telegram_user_id
        )
        self.connect()
        self.execute_query(q)
        self.close()

    def get_user_state(self, telegram_user_id):
        USER_STATE_TEMPLATE = string.Template(
          "SELECT state, room_id"
          " FROM users"
          " WHERE telegram_user_id=$telegram_user_id"
        )
        q = USER_STATE_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        state = self.execute_query(q, return_values=True)[0][0]
        room_id = self.execute_query(q, return_values=True)[0][1]
        self.close()
        return state, room_id

    def set_user_state(self, telegram_user_id, state):
        USER_STATE_TEMPLATE = string.Template(
          "UPDATE users"
          " SET state = '$state'"
          " WHERE telegram_user_id=$telegram_user_id"
        )
        q = USER_STATE_TEMPLATE.substitute(telegram_user_id=telegram_user_id, state=state)
        self.connect()
        state = self.execute_query(q, return_values=True)[0][0]
        self.close()
        return state

##########################################
# ОПЕРАЦИИ С КОМНАТОЙ
##########################################

    def create_room(self, telegram_user_id, room_name, user_query):
        ROOM_CREATE_TEMPLATE = string.Template(
          "WITH counted AS ("
          "  SELECT nextval('rooms') AS rooms_count"
          " )"
          " UPDATE users"
          " SET"
          " room_id = c.rooms_count,"
          " room_name = '$room_name',"
          " state = 'in_room',"
          " user_query = '$user_query',"
          " room_joined_time = '$now'"
          " FROM counted c"
          " WHERE users.telegram_user_id = $user_id"
        )
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
        q = ROOM_CREATE_TEMPLATE.substitute(room_name=room_name, user_id=telegram_user_id, user_query=user_query, now=now)
        self.connect()
        self.execute_query(q)
        room_id = self.execute_query("SELECT currval('rooms')", return_values=True)[0]
        self.close()
        return room_id[0]

    def get_room_by_room_names(self, room_names: List[str]) -> List[Tuple[int, str]]:
        """
        Возвращает список комнат. Их айди и имя
        Пример: [(6, 'Гипотеза о потенциальных клиенты')]
        """

        room_names_string: str = str(room_names)[1:-1]
        ROOM_GET_TEMPLATE = string.Template(
          "SELECT DISTINCT room_id, room_name"
          " FROM users"
          " WHERE users.room_name IN ($room_names_string)"
        )

        q = ROOM_GET_TEMPLATE.substitute(room_names_string=room_names_string)
        self.connect()
        result = self.execute_query(q, return_values=True)
        self.close()
        return result

    def get_all_rooms_by_room_name(self, theme_name):
        ROOM_GET_TEMPLATE = string.Template(
          "SELECT DISTINCT room_id, count(room_id) AS users_amount"
          " FROM users"
          " GROUP BY room_id, room_name"
          " HAVING users.room_name = '$room_name'"
        )
        q = ROOM_GET_TEMPLATE.substitute(room_name=theme_name)
        self.connect()
        idxs = self.execute_query(q, return_values=True)
        for idx in idxs:
            if 1 != 1: #idx[-1] > 5: -- users in room limit
                continue
            else:
                return idx[0]
        else:
            return None

    def append_to_room(self, room_id, room_name, telegram_user_id, user_query):
        ROOM_APPEND_TEMPLATE = string.Template(
          "UPDATE users"
          " SET"
          " room_id = $room_id,"
          " room_name = '$room_name',"
          " user_query = '$user_query',"
          " room_joined_time = '$now',"
          " learn_time_estimate = 60,"
          " estimator_number = 0,"
          " state = 'in_room'"
          " WHERE users.telegram_user_id = $user_id"
        )
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')
        q = ROOM_APPEND_TEMPLATE.substitute(room_id=room_id, 
                                            room_name=room_name, 
                                            user_id=telegram_user_id,
                                            user_query=user_query,
                                            now=now)
        self.connect()
        self.execute_query(q)
        self.close()

    def get_room_members(self, room_id) -> list:
        ROOM_MEMBERS_TEMPLATE = string.Template(
          "SELECT telegram_user_id"
          " FROM users"
          " WHERE room_id=$room_id"
        )
        q = ROOM_MEMBERS_TEMPLATE.substitute(room_id=room_id)
        self.connect()
        ids = self.execute_query(q, return_values=True)
        self.close()
        return [i[0] for i in ids]

    def get_room_members_with_queries(self, room_id, telegram_user_id) -> list:
        ROOM_MEMBERS_TEMPLATE = string.Template(
          "SELECT telegram_name, user_query"
          " FROM users"
          " WHERE room_id=$room_id AND $telegram_user_id != telegram_user_id"
        )
        q = ROOM_MEMBERS_TEMPLATE.substitute(room_id=room_id, telegram_user_id=telegram_user_id)
        self.connect()
        ids = self.execute_query(q, return_values=True)
        self.close()
        return ids
    
    def get_room_by_user(self, telegram_user_id):
        ROOM_MEMBERS_TEMPLATE = string.Template(
          "SELECT room_id"
          " FROM users"
          " WHERE telegram_user_id=$telegram_user_id"
        )
        q = ROOM_MEMBERS_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        room = self.execute_query(q, return_values=True)
        self.close()
        if room:
            return room[0][0]
        else:
            return None

    def leave_room(self, telegram_user_id):
        LEAVE_ROOM_TEMPLATE = string.Template(
            "UPDATE users"
            " SET"
            " room_id=NULL,"
            " room_name=NULL,"
            " state='no_room',"
            " user_query=NULL"
            " WHERE telegram_user_id=$telegram_user_id"
        )
        q = LEAVE_ROOM_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        self.execute_query(q)
        self.close()

    def get_room_name_by_user(self, telegram_user_id):
        ROOM_NAME_TEMPLATE = string.Template(
            "SELECT room_name"
            " FROM users"
            " WHERE telegram_user_id=$telegram_user_id"
        )
        q = ROOM_NAME_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        room_name = self.execute_query(q, return_values=True)[0][0]
        self.close()
        return room_name

##########################################
# Другое
##########################################

    def get_user_theme(self, telegram_user_id):
        USER_NAME_TEMPLATE = string.Template(
          "SELECT user_query"
          " FROM users"
          " WHERE telegram_user_id=$telegram_user_id"
        )
        q = USER_NAME_TEMPLATE.substitute(telegram_user_id=telegram_user_id)
        self.connect()
        user_query = self.execute_query(q, return_values=True)[0][0]
        self.close()
        return user_query


db = Database(host="rc1b-kujwcjnpecm2ix4k.mdb.yandexcloud.net",
              db='hack',
              username="hack",
              password="hackhack",
              port=6432)


# print(db.get_room_by_room_names(room_names=["Гипотеза о потенциальных клиенты"]))