import json
import requests


url = 'http://84.201.177.215:5000/themes?'
params = 'query={}&topk={}'

def get_theme(text: str, topk: int):
    request = url+params.format(text, topk)
    resp = requests.get(request)
    response = json.loads(requests.get(request).text)
    themes = [(item['theme'], item['similarity']) for item in response['themes']]
    return themes