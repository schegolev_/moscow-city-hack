import functools

import telebot
import psycopg2

from database import db


def start_handler(message, bot: telebot.TeleBot):

    TEXT = """
    Привет, рады тебя видеть 😉.
Прямо сейчас 12345 человек учатся разному.
Пиши, какой темой или задачей ты занимаешься и мы закинем тебя в подходящую боталку!

Для просмотра списка команда введи /help.

Если хочешь начать ботать какую-то тему, просто напиши её 📚.
    """

    bot.send_message(message.from_user.id, TEXT)

def help_handler(message, bot: telebot.TeleBot):
    """
    Отправляем инструкцию только запросившему пользователю
    :param message:
    :param bot:
    :return:
    """

    TEXT = """
    Список команд:
🔸 /leave_room: Выйти из комнаты.
🔸 /which_room: Название комнаты, в которой вы находитесь.
🔸 /rating: Показывает ваш рейтинг.
        
‼️ Но сначала зарегистрируйся с помощью команды /register.

Если хочешь начать ботать какую-то тему, просто напиши её 📚.
    """
    bot.send_message(message.from_user.id, TEXT)


def register_handler(message, bot: telebot.TeleBot):
    """
    Регистрируем пользователя по его telegram id
    """

    if message.text == '/register':

        if not db.user_exists(telegram_user_id=message.from_user.id):

            TEXT = """
                Как тебя называть в сервисе? """
            msg = bot.send_message(message.from_user.id, text=TEXT, reply_to_message_id=message.id)

            # functools.partial нужна, чтобы прокинуть аргумент bot=bot
            # next step пойдет уже в ветку "else"
            bot.register_next_step_handler(
                msg,
                functools.partial(register_handler, bot=bot)
            )

        else:

            bot.reply_to(message, text="Ты уже зарегистрирован. Напиши тему, чтобы начать ботать 📚")

    else:

        TEXT = """
        Зарегистрировали 🎉
Напиши нам, что сейчас изучаешь и мы закинем тебя в боталку 
        """
        try:
            db.create_user(telegram_user_id=message.from_user.id, telegram_name=message.text)
            bot.reply_to(message, text=TEXT)
        except psycopg2.errors.UniqueViolation:
            bot.reply_to(message, text="Ты уже зарегистрирован. Напиши тему, чтобы начать ботать 📚")
        except Exception:
            bot.reply_to(message, text="Что то сломалось у нас. Попробуй позже.")


def which_room_handler(message, bot: telebot.TeleBot):
    """
    Просмотр названия комнаты. 
    """
    if bot.get_state(message.from_user.id) != "no_room":
        try:
            room_name = db.get_room_name_by_user(telegram_user_id=message.from_user.id)
            TEXT = "Ты находишься в комнате: 🏡 {}".format(room_name)
            bot.send_message(message.from_user.id, TEXT)
        except Exception:
            bot.reply_to(message, text="Что то сломалось у нас. Попробуй позже.")
    else:
        TEXT = """
        В данный момент ты не подключен к комнате 😔. Напиши тему и начнём ботать!
        """
        bot.send_message(message.from_user.id, TEXT)



def leave_room_handler(message, bot: telebot.TeleBot):
    """
    Команда выхода из комнаты
    """
    if bot.get_state(message.from_user.id) != "no_room":
        # try:
        db.leave_room(telegram_user_id=message.from_user.id)
        bot.set_state(message.from_user.id, "no_room")
        TEXT = """
        Ты хорошо поботал! Возвращайся поскорее 😉.
        """
        bot.send_message(message.from_user.id, TEXT)
        # except Exception:
        #     bot.reply_to(message, text="Что то сломалось у нас. Попробуй позже.")
    else:
        TEXT = """
        В данный момент ты не подключен к комнате 😔. Напиши тему и начнём ботать!
        """
        bot.send_message(message.from_user.id, TEXT)


def rating_handler(message, bot: telebot.TeleBot):
    """
    Команда показа рейтинга
    """

    TEXT = """
    Ваш рейтинг 
🧠 Ответы: {}
🎓 Вопросы: {}
    """

    user_info = db.get_user(telegram_user_id=message.from_user.id)
    bot.send_message(message.from_user.id, TEXT.format(str(user_info[-1]), str(user_info[-2])))
