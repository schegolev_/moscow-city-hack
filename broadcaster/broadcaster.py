import telebot


from command_handlers import (
    start_handler,
    help_handler,
    register_handler,
    which_room_handler,
    leave_room_handler,
    rating_handler
)

from message_handlers import broadcast

token = "5302941769:AAGNQ8PH2ebTx38rXkNxWDWcdGI3mgizyyg"
bot = telebot.TeleBot(token)


bot.register_message_handler(callback=start_handler,
                             commands=['start'],
                             pass_bot=True)

bot.register_message_handler(callback=help_handler, 
                             commands=['help'], 
                             content_types=['text'], 
                             pass_bot=True)
                             
bot.register_message_handler(callback=register_handler,
                             commands=['register'],
                             pass_bot=True)

bot.register_message_handler(callback=which_room_handler,
                             commands=['which_room'],
                             pass_bot=True)

bot.register_message_handler(callback=leave_room_handler,
                             commands=['leave_room'],
                             pass_bot=True)

bot.register_message_handler(callback=rating_handler,
                             commands=['rating'],
                             pass_bot=True)

# Наш основной хендлер
bot.register_message_handler(callback=broadcast,
                             content_types=['text', 'voice', 'photo'],
                             pass_bot=True)


bot.infinity_polling()

