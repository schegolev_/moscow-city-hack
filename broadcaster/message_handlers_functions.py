import functools

import telebot
import functools

from typing import List
from database import db
from indexer import get_theme


def get_new_mean(current_value, estimator_number, new_estimate):
    current_sum = current_value*estimator_number
    new_mean = int((current_sum+new_estimate) / (estimator_number+1))
    return new_mean


def send_all(message: telebot.types.Message, bot: telebot.TeleBot, room_id: int, text: str):
    user_ids = db.get_room_members(room_id)
    # Удаляем себя из рассылки
    user_ids.remove(message.from_user.id)
    for user_id in user_ids:
        bot.send_message(user_id, text)


def notify_all(message: telebot.types.Message, bot: telebot.TeleBot, room_id: int):
    TEXT = """
    К вам в комнату добавился {}, greeting!
    """
    user_name = db.get_user(message.from_user.id)[1]
    send_all(message, bot, room_id, TEXT.format(user_name))


def add_user_to_room(message: telebot.types.Message, bot: telebot.TeleBot, rooms):
    room_id, room_name = rooms[0]

    TEXT = """
    Мы нашли для вас группу! Можете начинать ботать!
    """
    db.append_to_room(room_id=room_id,
                      room_name=room_name,
                      telegram_user_id=message.from_user.id,
                      user_query=message.text)
    notify_all(message, bot, room_id)
    bot.set_state(message.from_user.id, room_id)
    bot.send_message(message.from_user.id, TEXT)
    ask_estimate(message, bot, room_id, message.text)


    # Теперь выведем вступившему всех участников комнаты
    members_string = "С вами в группе уже:\n"
    for member in db.get_room_members_with_queries(room_id=room_id, telegram_user_id=message.from_user.id):
        s = f"👨‍🎓{member[0]} ботает '{member[1]}' уже 12 часов подряд!\n"
        members_string = members_string + s
    members_string = members_string + "\nМожешь поспрашивать у них если знаешь их темы"
    bot.send_message(message.from_user.id, members_string)


def suggest_to_create_new_room(message: telebot.types.Message, bot: telebot.TeleBot, topk: List[str]):
    def make_topk_string(topk):
        s = ""
        for i, theme in enumerate(topk):
            s = s + f"{str(i)}. {theme}\n"
        return s

    msg = bot.send_message(message.from_user.id,
                           "По вашей теме еще нет комнат. Хотите создать?"
                           "Можно выбрать тему цифрой или написать 'нет'.\n"
                           "Список тем:\n" + make_topk_string(topk))

    bot.register_next_step_handler(msg, functools.partial(user_creates_new_room_handler, bot=bot, topk=topk))



def user_creates_new_room_handler(message: telebot.types.Message, bot: telebot.TeleBot, topk: List[str]):
    """
    Диалог по созданию комнаты.
    Тут пользователь либо решает создать комнату либо нет.
    Если он решает создать, то приходит порядоквый номер от 0 до 4
    """

    # def handler_create_new_room(message: telebot.types.Message, bot: telebot.TeleBot):

    if message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         "Окей, тогда может попробуешь переформулировать?",
                         reply_to_message_id=message.id)

    # Нам прислали валидный номер. Можем создать комнату
    elif (len(message.text.lower()) == 1) & str.isdigit(message.text[0]) & (int(message.text[0]) <= 4):
        room_name = topk[int(message.text[0])]
        user_query = bot.get_state(user_id=message.from_user.id)
        db.create_room(telegram_user_id=message.from_user.id,
                       room_name=room_name,
                       user_query=user_query)
        msg = bot.send_message(message.from_user.id,
                               "Круто! Создали комнату",
                               reply_to_message_id=message.id)

    else:
        msg = bot.send_message(message.from_user.id,
                               "Можешь плиз ответить ДА или НЕТ",
                               reply_to_message_id=message.id)
        bot.register_next_step_handler(msg, functools.partial(user_creates_new_room_handler, bot=bot, topk=topk))




def add_user_to_room_or_suggest_create_new_or_decline(message: telebot.types.Message, bot: telebot.TeleBot):
    # Закешим то, что написал пользователь, чтобы сохранить это как user_query

    threshold = 0.45
    topk = [k[0] for k in get_theme(message.text, 5) if k[1] > threshold]

    # Ничего близкого не было - предлагаем пользаку выбрать новую тему
    if not topk:
        # Сбросим кеш - не понадобится
        bot.send_message(message.from_user.id, "Мы пока не поддерживаем вашу тему, попробуйте переформулировать")
    # В принципе есть схожие темы. Найдем пользователю конкретную комнаиту или предложим создать
    else:
        assert isinstance(topk, list), "topk должен быть списком"
        rooms: List = db.get_room_by_room_names(room_names=topk)

        # Все четко, по запросу нашлась всего лишь одна комната. Попробуем добавить туда пользователя
        if len(rooms) == 1:
            add_user_to_room(message, bot, rooms)

        # Пороги прошли, но комнаты не нашлось. Предложим пользователю создать новую
        elif len(rooms) == 0:
            bot.set_state(user_id=message.from_user.id,
                          state=message.text)
            suggest_to_create_new_room(message, bot, topk)

        else:
            bot.send_message(message.from_user.id, "Вау. У нас что то совсем пошло не так... #КОД1")


def ask_estimate(message: telebot.types.Message, bot: telebot.TeleBot, room_id: int, user_query: str):
    TEXT = """
    {} решил ботать тему {}, как вы думаете, сколько минут потребуется для ее изучения?
    """
    user_name = db.get_user(message.from_user.id)[1]
    user_ids = db.get_room_members(room_id)
    # Удаляем себя из рассылки
    user_ids.remove(message.from_user.id)
    for user_id in user_ids:
        msg = bot.send_message(user_id, TEXT.format(user_name, user_query))
        bot.register_next_step_handler(
            msg,
            functools.partial(
                set_estimate, bot=bot, 
                estimating_user_id=message.from_user.id)
        )


def set_estimate(message: telebot.types.Message, bot: telebot.TeleBot,
                 estimating_user_id: str):
    new_estimation = int(message.text)
    user_row = db.get_user(estimating_user_id)
    current_time_estimation = user_row[6]
    estimator_number = user_row[7]
    new_mean = get_new_mean(current_time_estimation, estimator_number, new_estimation)
    db.update_user_time_estimation(estimating_user_id, new_mean, estimator_number+1)
    
    TEXT = """
    Спасибо, мы учтем вашу оценку!
    """
    bot.send_message(message.from_user.id, TEXT)
