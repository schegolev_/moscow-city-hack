# Theme Indexer service

Service with predefined educational themes. Based on query it returns top K similar themes with its domain.

## Install 

### Build from Source

Just install all requirements and stark flask server

```
pip install -r requirements.txt
cd src
FLASK_APP=service flask run --host=0.0.0.0
```

### Docker 

```
docker build -t theme-indexer:latest .
docker run -d --restart always --network host theme-indexer:latest
```

## Usage

API methods:

* `/health`
  returns status code 200 if service if alive

* `/theme`
  * `query` (str) — new theme query
  * `topk` (int) — number of similar themes
  
  * returns answer with this scheme:
    ```
    {
      "items": [
        {"domain": str, "theme": str}
      ]
    }
    ```
  
  
