from index import ThemeIndexer
from flask import Flask, Response, request

app = Flask(__name__)

index = ThemeIndexer('data/')

@app.route('/themes')
def get_themes():
    query = request.args.get('query')
    topk = request.args.get('topk')
    answers = index.find(query, int(topk))
    response = {
            "themes": [{"domain": ans[0][0], "theme": ans[0][1], "similarity": ans[1]} for ans in answers]
        }
    return response

@app.route('/health')
def healthcheck():
    return Response(status=200)
