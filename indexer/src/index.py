import os
import json
import torch
from transformers import AutoTokenizer, AutoModel


def get_embedder():
    return {
            "tokenizer": AutoTokenizer.from_pretrained("cointegrated/rubert-tiny2"),
            "model": AutoModel.from_pretrained("cointegrated/rubert-tiny2")
        }


def get_json(folder):
    files = [f for f in os.listdir(folder) if '.json' in f]
    data = {}
    for f in files:
        with open(folder+f, 'r') as handler:
            data[f.split('.')[0]] = json.load(handler)
    return data


class ThemeIndexer:
    def __init__(self, folder: str):
        self.index = []
        self.kv = {}
        embedder = get_embedder()
        self.model = embedder['model']
        self.tokenizer = embedder['tokenizer']
        jsons = get_json(folder)
        for j in jsons.keys():
            for k, v in jsons[j].items():
                emb = self.embed_bert_cls(' '.join([k.lower()+': ' for _ in range(10)])+v.lower())
                self.index.append(emb.unsqueeze(0))
                self.kv[len(self.index)-1] = (j, k)
        self.index = torch.cat(self.index)
        self.sim = torch.nn.CosineSimilarity(dim=1)

    def embed_bert_cls(self, text: str):
        t = self.tokenizer(text, padding=True, truncation=True, return_tensors='pt')
        with torch.no_grad():
            model_output = self.model(**{k: v.to(self.model.device) for k, v in t.items()})
        embeddings = model_output.last_hidden_state[:, 0, :]
        embeddings = torch.nn.functional.normalize(embeddings)
        return embeddings[0].cpu()

    def find(self, query: str, topk=5) -> tuple:
        topk = min(topk, self.index.shape[0])
        query_emb = self.embed_bert_cls(query.lower()).unsqueeze(0)
        sims = self.sim(query_emb, self.index).numpy()
        candidates = torch.topk(self.sim(query_emb, self.index), topk)
        idxs, values = candidates.indices, candidates.values
        return [(self.kv[idx.item()], values[k].item()) for k, idx in enumerate(idxs)]

