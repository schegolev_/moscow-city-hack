# Moscow City Hack
## StatusCode500
## Трэк 5: бот-помощник для студентов

### Работающее решение

Бот развернут и работает по ссылке: [statusCode500_bot](https://t.me/statusCode500_bot)

### Описание

**Broadcaster** - сервис, ответственный за движок общения студентов внутри боталки. Он делает так, чтобы твои сообщения доходили до всех в боталке, а также, чтобы ты видел сообщения других студентов из боталки

**Facilitator** - Логика фасилитатора Боталки, способная коммуницировать со студентами и модерировать их

**Indexer** — сервис, выдающий K ближайщих тем в тому, что студент хочет начать ботать

### Installation

Рекомендуем запускать на такой тачке:
* Ubuntu 20.04.4 LTS
* 4 CPU
* 16 GB RAM

```
cd broadcaster && docker build -t broadcaster:latest . && cd ..
cd indexer && docker build -t theme-indexer:latest . && cd ..
cd facilitator && docker build -t facilitator:latest . && cd ..

docker-compose up -d
```

PROFIT!

### Team

Made by: 
* [waryak](https://t.me/waryak)
* [schegoleev](https://t.me/schegoleev)
* [ivan_moso](https://t.me/ivan_moso)

#### P.S.

Посвящается Виктору Задябину, мы с тобой!